from nose.tools import assert_equal
import aims

def test1():
    list = [4, 4]
    abs = aims.std(list)
    exp = 0
    assert_equal(abs, exp)

    
def test2():
    list = [-3, -4]
    abs = aims.std(list)
    exp = 0.5
    assert_equal(abs, exp)

def test3():
    list = [0.2, 0.4]
    abs = aims.std(list)
    exp = 0.1
    assert_equal(abs, exp)

def test4():
    list = [-2, 4]
    abs = aims.std(list)
    exp = 3
    assert_equal(abs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00215']
    abs = aims.avg_range(files)
    exp = 5.0
    assert_equal(abs, exp)

def test_avg2():
    files = ['data/bert/audioresult-00317']
    abs = aims.avg_range(files)
    exp = 6.0
    assert_equal(abs, exp)

def test_avg3():
    files = ['data/bert/audioresult-00384']
    abs = aims.avg_range(files)
    exp = 1.0
    assert_equal(abs, exp)

def test_avg4():
    files = ['data/bert/audioresult-00557']
    abs = aims.avg_range(files)
    exp = 2.0
    assert_equal(abs, exp)
