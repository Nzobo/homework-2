import numpy as np

if __name__=='__main__':    
    print "Welcome to the AIMS module"

def std(x):
    xbar = np.mean(x)             #finding mean
    devsum=0.0                      #initializing sum
    
    for i in range(len(x)):    
        dev2 = (x[i] - xbar)**2
        devsum += dev2               #findind the sum of (x - xbar)**
    return np.sqrt(devsum/len(x))



def avg_range(lst):              #defining average_range funcion
    files=[]                        #Initializing empty list of files
    ranges=[]                         #Initializing empty list of ranges
    for location in lst:
        files.append(open(location))      #oppening location and appending the values in files
    
    for entry in files:
        for line in entry:
            if 'Range' in line:               #Searching Range
                ranges.append(float(line[7]))     #Appending the Range to ranges
    return sum(ranges)/len(ranges)              #Finding the average of ranges





###########################################################################################################


########  I ackowledge the contribution of Robin in development of this code.....thank you
